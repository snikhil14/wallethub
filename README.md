# BDD Framework

This repository provides some working boilerplate code for building automated test suites for UI-based testing with Selenium.

# Below are some of the features of this test framework:

* Modular Design
* Maven based framework
* Extent Report for report
* Report Generation (cucumber-reporting)
* Helper class to handle web component such as (click,sendkeys etc)
* POM
* Different browser support (chrome,firefox)
* Java 8(lambda)
* Slack integration

## Documentation

* [Predefined steps](https://github.com/selenium-cucumber/selenium-cucumber-java/blob/master/doc/canned_steps.md)
* [Installation](https://github.com/selenium-cucumber/selenium-cucumber-java/blob/master/doc/installation.md)
* [Install IntelliJ](https://www.jetbrains.com/idea/download/#section=linux) for easy use of lambda
* Configure and Install Following Intellij Plugins
 File -> Setting -> Plugins -> Browser Repositories->
* Cucumber for Java 
* Gherkin 
* Maven Integration 

# Getting a Slack API access token(optional)

You can obtain a Slack API access token for your workspace by following the steps below:

* In your Slack Workspace, click the Apps section.
* In the Apps page, click Manage apps.
* The App Directory page shows up, in this page, make a search using the keyword “bots” in the top text box Search App Directory.
* Click Bots app > Add configuration.
* Set Username and click Add bot integration.
* You’ll get the API access token in Integration Settings.


# Setting up and running tests

* Go to Project >src >test >java >feature >Login.feature
	* Add your valid facebook username and password for run test
* Open project command line and run below command
	* mvn clean compile test -Dbrowser=chrome 
* If you want to sent message to slack configure below things.
	* Update slack message constants from Project >src> main >java >constants >Constant
	* Uncomment the onFinish method from Project >src> main >java >utility> HookHelper 

That's all folks. Happy testing!
