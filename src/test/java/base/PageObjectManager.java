package test.java.base;

import main.java.Pages.HomePage;
import main.java.Pages.LoginPage;
import main.java.Pages.ProfilePage;
import main.java.Pages.ReviewPage;

public class PageObjectManager {

    private LoginPage loginPage;
    private HomePage homePage;
    private ProfilePage profilePage;
    private ReviewPage reviewPage;

    public LoginPage getLoginPage() {
        if (loginPage == null) {
            loginPage = new LoginPage();
        }
        return loginPage;
    }

    public HomePage getHomePage() {
        if (homePage == null) {
            homePage = new HomePage();
        }
        return homePage;
    }

    public ProfilePage getProfilePage() {
        if (profilePage == null) {
            profilePage = new ProfilePage();
        }
        return profilePage;
    }

    public ReviewPage getReviewPage() {
        if (reviewPage == null) {
            reviewPage = new ReviewPage();
        }
        return reviewPage;
    }
}
