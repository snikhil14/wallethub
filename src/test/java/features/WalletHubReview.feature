Feature: Write review
  This feature deals with the write review functionality of the wallethub application

  Scenario: Write review to wallethub
    Given Navigate to "https://wallethub.com/profile/test_insurance_company/"
    And I click on minimize advertise
    And I move to review stars
    And I click on star point
    And I click on select policy
    And I click on health from policy drop down
    And I insert review of text
    And I select rating
    And I click on submit button