package test.java.steps;

import cucumber.api.java.Before;
import cucumber.api.java8.En;
import main.java.Pages.ProfilePage;
import main.java.Pages.ReviewPage;
import org.apache.commons.lang.RandomStringUtils;
import test.java.base.PageObjectManager;


public class ReviewTest implements En {

    PageObjectManager pageObjectManager = new PageObjectManager();
    ProfilePage profilePage;
    ReviewPage reviewPage;

    @Before
    public void beforeMethod() {
        profilePage = pageObjectManager.getProfilePage();
        reviewPage = pageObjectManager.getReviewPage();
    }

    public ReviewTest() {
        And("^I click on minimize advertise$", () -> {
            profilePage.clickOnAdvertise();
        });
        And("^I move to review stars$", () -> {
            try {
                profilePage.moveToReviews();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        And("^I click on star point$", () -> {
            profilePage.clickOnHover();
        });
        And("^I click on select policy$", () -> {
            reviewPage.clickOnSelectPolicy();
        });
        And("^I click on health from policy drop down$", () -> {
            reviewPage.clickOnSelectHealth();
        });
        And("^I select rating$", () -> {
            reviewPage.selectReating();
        });
        And("^I insert review of text$", () -> {
            String firstWord = RandomStringUtils.randomAlphanumeric(50);
            String secondWord = RandomStringUtils.randomAlphanumeric(50);
            String thirdWord = RandomStringUtils.randomAlphanumeric(50);
            String fourthWord = RandomStringUtils.randomAlphanumeric(50);
            String fifthWord = RandomStringUtils.randomAlphanumeric(50);
            String text = fifthWord + " " + secondWord + " " + thirdWord + " " + fourthWord + " " + fifthWord;
            reviewPage.sendReview(text);
        });
        And("^I click on submit button$", () -> {
            reviewPage.clickOnSubmitButton();
        });


    }
}
