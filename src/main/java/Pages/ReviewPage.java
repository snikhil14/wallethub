package main.java.Pages;

import main.java.utility.PagesHelper;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import test.java.base.BaseUtil;

public class ReviewPage extends BaseUtil {

    public ReviewPage() {
        PageFactory.initElements(driver, this);
    }

    @FindBy(css = "[id='reviewform']>div:nth-of-type(1)>div>div")
    WebElement selectPolicy;

    @FindBy(css = "[id='reviewform']>div:nth-of-type(1)>div>ul>li:nth-of-type(2)")
    WebElement selectHealth;

    @FindBy(css = "[id='overallRating']>a:nth-of-type(5)")
    WebElement rating;

    @FindBy(css = "[id='review-content']")
    WebElement reviewBox;

    @FindBy(css = ".submit>input")
    WebElement submitButton;

    public void clickOnSelectPolicy() {
        PagesHelper.click(selectPolicy);
    }

    public void clickOnSelectHealth() {
        PagesHelper.click(selectHealth);
    }

    public void selectReating(){
        PagesHelper.click(rating);
    }

    public void sendReview(String review) {
        PagesHelper.sendKeys(reviewBox, review);
    }

    public void clickOnSubmitButton() {
        PagesHelper.click(submitButton);
    }
}
