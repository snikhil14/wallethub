package main.java.Pages;

import main.java.utility.PagesHelper;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import test.java.base.BaseUtil;

public class ProfilePage extends BaseUtil {

    public ProfilePage() {
        PageFactory.initElements(driver, this);
    }

    @FindBy(css = "[id='footer_cta']>span>span>i:nth-of-type(2)")
    WebElement advertise;

    @FindBy(css = "[class^='wh-rating rating_']")
    WebElement reviewStars;

    @FindBy(css = ".wh-rating-choices-holder>a:nth-of-type(5)")
    WebElement fifthStar;

    public void clickOnAdvertise() {
        PagesHelper.click(advertise);
    }

    public void moveToReviews() throws InterruptedException {
        PagesHelper.moveToElement(reviewStars);
    }

    public void clickOnHover() {
        PagesHelper.click(fifthStar);
    }

}
