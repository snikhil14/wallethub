package main.java.Pages;

import main.java.utility.PagesHelper;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import test.java.base.BaseUtil;

public class HomePage extends BaseUtil {

    public HomePage() {
        PageFactory.initElements(driver, this);
    }

    @FindBy(css = "[data-click='profile_icon']")
    WebElement profileIcon;


    @FindBy(css = "[name='xhpc_message']")
    WebElement messageBox;

    @FindBy(css = "[data-testid='react-composer-post-button']")
    WebElement postButton;

    public boolean isProfileIconDisplayed() {
        return PagesHelper.isDisplayed(profileIcon);
    }


    public void sendMessage(String text) {
        PagesHelper.sendKeys(messageBox, text);
    }

    public void clickOnPostButton() {
        PagesHelper.click(postButton);
    }


}
